1. Fill `data.json`.
2. Place template files in directory: `contract-templates`. <b>Templates must use following pattern: `{VARIABLE_NAME}`!</b>
3. Fill `files.json` with template names.
4. Run `yarn build`.
5. Run `yarn start`.
6. Filled contracts will appear in `new-contracts` directory.
