import fs from 'fs';

const PizZip = require('pizzip');
const Docxtemplater = require('docxtemplater');

const JSON_DATA = 'data.json';
const JSON_FILES = 'files.json';

const TEMPLATES_DIR = 'contract-templates';
const OUT_DIR = 'new-contracts';

const data = JSON.parse(fs.readFileSync(JSON_DATA, 'utf8'));

const { files } = JSON.parse(fs.readFileSync(JSON_FILES, 'utf-8'));

files.forEach((file: string) => {
  const templateFile = `${TEMPLATES_DIR}/${file}`;
  const outFile = `${OUT_DIR}/${file}`;

  const content = fs.readFileSync(templateFile, 'binary');
  const zip = new PizZip(content);

  try {
    const doc = new Docxtemplater(zip);
    doc.setData(data);
    doc.render();

    const buf = doc.getZip().generate({ type: 'nodebuffer' });
    fs.writeFileSync(outFile, buf);

    console.log(`Created: ${outFile}`);
  } catch (error) {
    console.log(error);
  }
});

console.log('Finished.');
